# Stocks fo BrainBase

Test exersice for apply for a job in BrainBase.
Project has 4 simple UNIT tests.  

## Technologies used
    - [Angular](https://angular.io/)<br>
	- [TypeScript](https://www.typescriptlang.org/)<br>
	- [Rxjs](https://github.com/ReactiveX/rxjs)<br>
	- [Sass](http://sass-lang.com/)<br>

## How to install/build app

Run `npm install`. It will install all need packages and modules to run application.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/stocks`. The app will automatically redirect to `http://localhost:4200/stocks`. If so you are in right place.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Contact

* Name: Vladimir Kjahrenov
* Tel: +372 55 621 935
* E-mail: info@hurtlocker.pro
* Skype: hurtlockerpro
