import { TestBed } from '@angular/core/testing';

import { RestService } from './rest.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('RestService', () => {
  let restService: RestService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RestService],
      imports: [HttpClientTestingModule]
    });
    httpTestingController = TestBed.get(HttpTestingController);
    restService = TestBed.get(RestService);

  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should create StocksService', () => {
    expect(restService).toBeTruthy();
  });

  it('should connect to RESTful service', () => {

    const mockStocks  = [
      { symbol: 'AAPL',
        name: 'Apple',
        price: 1000
      },
      { symbol: 'MSFT',
        name: 'Microsoft',
        price: 250
      }
    ];

    restService.getStocks().subscribe( data => {
      expect(data[0].symbol).toEqual('AAPL');
    });

    const req = httpTestingController.expectOne(
      'https://staging-api.brainbase.com/stocks.php'
    );

    req.flush(mockStocks);
  });
});
