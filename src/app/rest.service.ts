import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {catchError, map, retry} from 'rxjs/operators';
import {IStocks} from './stocks/StocksStructureInterface';


const endpoint = 'https://staging-api.brainbase.com/stocks.php';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class RestService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }

  getStocks(): Observable<IStocks[]> {
    return this.http.get<IStocks[]>(endpoint).pipe(
      //map(this.extractData));
      retry(3),
      catchError(err => { return this.errorHandler(err); })
      );
  }

  errorHandler(error: HttpErrorResponse) {
    console.log('error occured');
    return throwError(error.message);
  }
}
