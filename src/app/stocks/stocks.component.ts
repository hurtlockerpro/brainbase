import {Component, OnInit} from '@angular/core';
import { RestService } from '../rest.service';

export interface Stocks {
  name: string;
  symbol: string;
  price: number;
  currentPrice: number;
  currentChange: string;
  futurePrice: number;
  futureChange: string;
}

@Component({
  selector: 'app-product',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.scss']
})
export class StocksComponent implements OnInit {

  title = 'Stocks of BrainBase';

  displayedColumns: string[] = ['name', 'symbol', 'price'];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  stocks: {} = [];
  dateValue: Date = new Date();

  constructor(public rest: RestService) { }

  ngOnInit() {
    this.getStocks();
  }

  getStocks() {
    this.stocks = [];
    this.rest.getStocks().subscribe((data: {}) => {
      this.stocks = data;
    });
  }

  getRandomNumber(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  Simulate() {

    // hack: delete 2 last columns of visible columns
    if (this.displayedColumns.length == 5) {
      this.columnsToDisplay.pop();
      this.columnsToDisplay.pop();

      this.displayedColumns.pop();
      this.displayedColumns.pop();
    }


    for (const key in this.stocks) {

      const isNegative =  this.getRandomNumber(0, 1);

      // calculate change in money value
      let futureChangeMoney = this.stocks[key].price * 0.1;
      if (isNegative == 1) futureChangeMoney = futureChangeMoney * -1;

      // current price in euros
      const currentPrice = this.stocks[key].price;

      // calculate future price value in euros
      const futurePrice = currentPrice + futureChangeMoney;
      this.stocks[key].futurePrice = futurePrice;

      // calculate change in percentage
      const futureChangePercentage = (futureChangeMoney * 100) / currentPrice;

      this.stocks[key].futureChange = futureChangeMoney.toFixed(2) + '/' + futureChangePercentage.toFixed(2) + '%';
    }

    this.displayedColumns.push('futurePrice', 'futureChange');
    this.columnsToDisplay.push('futurePrice', 'futureChange');

    // change date +1 and show it
    const tmpDate = new Date(Date.parse(document.getElementById('dateStyle').innerHTML));
    this.dateValue.setDate( tmpDate.getDate() + 1);
    document.getElementById('dateStyle').innerHTML = this.dateValue.toDateString();
  }

  isNegative(column: string, value: string){
    if (column === 'futureChange' && value) {
      if (value.substring(0, 1) === '-') {
        return true;
      } else {
        return false;
      }
    }
  }

}
