export interface IStocks {
  name: string;
  symbol: string;
  price: number;
  currentPrice: number;
  currentChange: string;
  futurePrice: number;
  futureChange: string;
}
